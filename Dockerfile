FROM azul/zulu-openjdk-alpine:11

VOLUME /tmp

ADD "./target/week-three-exam-0.0.1-SNAPSHOT.jar" "./app.jar"

CMD ["java", "-jar", "-Djava.security.egd=file:/dev/./urandom", "./app.jar"]