package ServiceTest;

import hk.com.novare.exam.weekthreeexam.model.AdminManagerModel;
import hk.com.novare.exam.weekthreeexam.model.EmployeeModel;
import hk.com.novare.exam.weekthreeexam.repository.AdminManagerRepository;
import hk.com.novare.exam.weekthreeexam.repository.EmployeeRepository;
import hk.com.novare.exam.weekthreeexam.service.AdminServiceImpl;
import hk.com.novare.exam.weekthreeexam.service.EmployeeService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;


import java.sql.Date;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = ServiceTest.class)
public class ServiceTest {
    List<EmployeeModel> expectedEmployee;
    EmployeeModel employeeModel;

    List<AdminManagerModel> expectedAdmin;
    AdminManagerModel adminManagerModel;


    @InjectMocks
    EmployeeService employeeService;


    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    AdminServiceImpl adminService;
    @Mock
    AdminManagerRepository adminManagerRepository;



    @BeforeEach
    public void test(){
        String dateHired ="2016-02-04";
        Date date1 = Date.valueOf(dateHired);

        String dateHired1 ="2019-04-06";
        Date date2 = Date.valueOf(dateHired1);

        String dateHire2 ="2018-07-01";
        Date date3 = Date.valueOf(dateHire2);


        employeeModel = new EmployeeModel(adminManagerModel,1,"Ryan","Llarena",date1,"ASE",20000);
        expectedEmployee = List.of(
                new EmployeeModel(adminManagerModel,2,"Raul","Bulanon",date2,"Software Engineer Lead",30000),
                new EmployeeModel(adminManagerModel,3,"Denmark","Cuevas",date3,"Technical Manager",440000),
                new EmployeeModel(adminManagerModel,1,"Bart","Simpson",date2,"CEO",50000));

        adminManagerModel = new AdminManagerModel(expectedEmployee,1,"bry","Rosales","SEQ");
        expectedAdmin = List.of(
                new AdminManagerModel(expectedEmployee,2,"Haryy","lou","SEG"),
                new AdminManagerModel(expectedEmployee,3,"Marl","Starl","SEG"));


    }

    @Test
    @DisplayName("Update Employee")
    public void updateEmployee(){
        Mockito.when(employeeRepository.findById(employeeModel.getId())).thenReturn(Optional.of(employeeModel));
        Mockito.when(employeeRepository.save(employeeModel)).thenReturn(employeeModel);
        employeeService.updateEmployee(employeeModel,1);
    }

    @Test
    @DisplayName("Delete Employee FirstName ")
    public void deleteEmployeeByFirstName(){
       employeeRepository.deleteEmployeeByName("Ryan");
    }

    @Test
    @DisplayName("Delete Single Employee")
    public void deleteSingleEmployee(){
        employeeRepository.deleteSingleEmployee(1);
    }


    @Test
    @DisplayName("Add New Employee")
    public void addNewEmployee(){
        Mockito.when(employeeRepository.save(employeeModel)).thenReturn(employeeModel);
        Assert.assertEquals(employeeModel,employeeService.addNewEmployee(employeeModel));
    }

    @Test
    @DisplayName("GetAllEmployee")
    public void getAllEmployee(){
        Mockito.when(employeeRepository.findAllEmployee()).thenReturn(expectedEmployee);
        Assert.assertEquals(expectedEmployee,employeeService.findAllEmployee());
    }

    @Test
    @DisplayName("Find Employee by Id")
    public void getEmployeeById(){
        Mockito.when(employeeRepository.findEmployeeById(2)).thenReturn(expectedEmployee);
        Assert.assertEquals(expectedEmployee,employeeService.findEmployeeById(2));

    }

    @Test
    @DisplayName("Find by Salary")
    public void getBySalary(){

        Mockito.when(employeeRepository.findBySalary(2,30000)).thenReturn(expectedEmployee);
        Assert.assertEquals(expectedEmployee,employeeService.findBySalary(2,30000));
        //employeeRepository.findBySalary(1,20000);
    }

    @Test
    @DisplayName("find by Name")
    public void getFindByName(){
        Mockito.when(employeeRepository.findByName("Raul")).thenReturn(expectedEmployee);
        Assert.assertEquals(expectedEmployee,employeeService.findByName("Raul"));
    }


    /*@Test
    @DisplayName("add New Admin")
    public void addAdmin(){
    Mockito.when(adminManagerRepository.save(adminManagerModel)).thenReturn(adminManagerModel);
    Assert.assertEquals(adminManagerModel,adminService.addNewAdmin(adminManagerModel));
        //adminManagerRepository.save(adminManagerModel);

    }*/

  /*  @Test
    @DisplayName("Get All Admin")
    public void getAllAdmin(){
        *//*Mockito.when(adminManagerRepository.findAll()).thenReturn(expectedAdmin);
        Assert.assertEquals(expectedAdmin,adminService.getAllAdmin());*//*
        adminManagerRepository.findAll();
        adminService.getAllAdmin();

    }*/


}
