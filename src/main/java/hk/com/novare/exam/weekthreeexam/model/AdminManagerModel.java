package hk.com.novare.exam.weekthreeexam.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table (name = "adminmanager")
public class AdminManagerModel {

    @JsonIgnore
    @OneToMany(mappedBy = "adminManagerModel")
    private List<EmployeeModel> employeeModel;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column (name = "id")
    private int id;

    @Column (name = "firstName")
    private String firstName;

    @Column (name = "lastName")
    private String lastName;

    @Column (name = "department")
    private String department;


    public AdminManagerModel(List<EmployeeModel> employeeModel, int id, String firstName, String lastName, String department) {
        this.employeeModel = employeeModel;
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
    }
}
