package hk.com.novare.exam.weekthreeexam.service;

import hk.com.novare.exam.weekthreeexam.model.AdminManagerModel;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AdminService {
    AdminManagerModel addNewAdmin(AdminManagerModel emp);
    List<AdminManagerModel> getAllAdmin();

}
