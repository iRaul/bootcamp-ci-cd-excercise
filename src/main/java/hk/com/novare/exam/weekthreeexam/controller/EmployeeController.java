package hk.com.novare.exam.weekthreeexam.controller;

import hk.com.novare.exam.weekthreeexam.model.AdminManagerModel;
import hk.com.novare.exam.weekthreeexam.model.EmployeeModel;
import hk.com.novare.exam.weekthreeexam.repository.EmployeeRepository;
import hk.com.novare.exam.weekthreeexam.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {

        @Autowired
        EmployeeRepository employeeRepository;

        @Autowired
        EmployeeService employeeService;
        @GetMapping(value = "/employee/all", produces = MediaType.APPLICATION_JSON_VALUE)
        public List<EmployeeModel> findAllEmployee(){
                return employeeService.findAllEmployee();
        }

        @GetMapping(value = "/employee/get/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
        public List<EmployeeModel> findEmployeeById(@PathVariable(value="id") int id){
                return employeeService.findEmployeeById(id);
        }

        @GetMapping(value = "/employee/get/salary",produces = MediaType.APPLICATION_JSON_VALUE)
        public List<EmployeeModel> findBySalary(@RequestParam int adminmanager_id, @RequestParam int salary){
                return employeeService.findBySalary(adminmanager_id,salary);
        }

        @GetMapping(value = "/employee/get/firstname",produces = MediaType.APPLICATION_JSON_VALUE)
        public List<EmployeeModel> findByName(@RequestParam String name){
                return employeeService.findByName(name);
        }

        //Application should allow deletion of multiple employees using wildcard filter (name,employment status)
        @DeleteMapping("employee/deleteLike/{name}")
        public void deleteEmployeeByName(@PathVariable(value="name") String name){
                employeeService.deleteEmployeeByName(name);
        }

        //Application should allow deletion of a single employee
        @DeleteMapping("employee/delete/{id}")
        public void deleteSingleEmployee(@PathVariable(value="id") int id){
                employeeService.deleteSingleEmployee(id);
        }

        //Application should allow update of department information (information update is not fixed. Eg. update head only,
        //update code & name, update location only, etc., head)
        @PutMapping("employee/update/{id}")
        public void updateEmployee(@RequestBody EmployeeModel employeeModel,@PathVariable("id") int id){
               employeeService.updateEmployee( employeeModel, id);
        }
        @PostMapping("/employee/new")
        public EmployeeModel addNewEmployee(@RequestBody EmployeeModel model){
                return employeeService.addNewEmployee(model);
        }
        /*@GetMapping(value= " /employee/bulk",produces = MediaType.APPLICATION_JSON_VALUE)
        public void uploadBulkEmployee() {
                List<EmployeeModel> employeeModelList = new ArrayList<>();
                int counter = 0;
                String path = "C:/Users/Raul Bulanon/Desktop/bootcamp-week-three-exam/src/main/resources/employee.csv";
                try {
                        File file = new File(path);
                        FileReader fr = new FileReader(file);
                        BufferedReader br = new BufferedReader(fr);
                        String line = "";
                        String[] tempArr;
                        while((line = br.readLine()) != null) {
                                tempArr = line.split(",");
                                for(String tempStr : tempArr) {
                                        System.out.print(tempStr + " ");
                                }
                                counter++;
                                if(counter > 1){
                                        System.out.println();
                                        EmployeeModel employeeModel = new EmployeeModel();
                                        AdminManagerModel adminManagerModel = new AdminManagerModel();
                                        employeeModel.setId((Integer.parseInt(tempArr[0])));
                                        employeeModel.setFirstName(tempArr[1].trim());
                                        employeeModel.setLastName(tempArr[2].trim());
                                        employeeModel.setDateHired((stringToDateConverter(tempArr[3])));
                                        employeeModel.setPosition(tempArr[4].trim());
                                        adminManagerModel.setId(Integer.parseInt(tempArr[5].trim()));
                                        employeeModel.setAdminManagerModel(adminManagerModel);
                                        employeeModelList.add(employeeModel);
                                }
                        }
                        br.close();
                } catch(IOException | ParseException ioe) {
                        ioe.printStackTrace();
                }

                employeeModelList.forEach(System.out::println);
                employeeRepository.saveAll(employeeModelList);


        }


        public static Date stringToDateConverter(String date) throws ParseException {

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date1 = dateFormat.parse(date);
                return date1;


        }*/
}

