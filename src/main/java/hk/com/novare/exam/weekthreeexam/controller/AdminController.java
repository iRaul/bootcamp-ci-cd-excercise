package hk.com.novare.exam.weekthreeexam.controller;

import hk.com.novare.exam.weekthreeexam.model.AdminManagerModel;
import hk.com.novare.exam.weekthreeexam.service.AdminServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class AdminController {
    @Autowired
    private final AdminServiceImpl adminServiceImpl;

    public AdminController(AdminServiceImpl adminServiceImpl) {
        this.adminServiceImpl = adminServiceImpl;
    }

    @PostMapping("/admin/new")
    public AdminManagerModel addNewEmployee(@RequestBody AdminManagerModel model){
        return adminServiceImpl.addNewAdmin(model);
    }
    @GetMapping(value = "/admin/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AdminManagerModel> getAllEmployee() {
        return adminServiceImpl.getAllAdmin();
    }
}
