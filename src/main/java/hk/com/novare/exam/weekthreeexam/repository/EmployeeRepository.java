package hk.com.novare.exam.weekthreeexam.repository;

import hk.com.novare.exam.weekthreeexam.model.EmployeeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeModel, Integer> {

    // Delete
    @Transactional
    @Modifying
    @Query("DELETE FROM employee_profile WHERE first_name LIKE :name%")
    void deleteEmployeeByName(@Param("name")String name);

    @Transactional
    @Modifying
    @Query("DELETE FROM employee_profile WHERE id = :id")
    void deleteSingleEmployee(@Param("id") int id);

    //Update
    @Modifying
    @Query("UPDATE employee_profile SET position = ?1," +
            " date_hired = ?2," +
            " first_name = ?3," +
            " last_name = ?4" +
            " WHERE id = ?5")
    void updateEmployee(int id);

    @Modifying
    @Query(value ="SELECT * FROM employee_profile E INNER JOIN adminmanager A ON E.adminmanager_id = A.id",nativeQuery = true)
    public List<EmployeeModel> findAllEmployee();

    @Modifying
    @Query(value ="SELECT * FROM employee_profile E INNER JOIN adminmanager A ON E.adminmanager_id = A.id WHERE E.id=:id",nativeQuery = true)
    public List<EmployeeModel> findEmployeeById(@Param("id") int id);

    @Modifying
    @Query(value ="SELECT * FROM employee_profile E INNER JOIN adminmanager A ON E.adminmanager_id = A.id " +
            "WHERE  adminmanager_id=:id AND salary >= :salary",nativeQuery = true)
    public List<EmployeeModel> findBySalary(@Param("id") int id, @Param("salary") int salary);

    @Modifying
    //@Query("select u from User u where u.firstname like %?1")
    @Query(value="SELECT * FROM employee_profile E INNER JOIN adminmanager A ON E.adminmanager_id = A.id WHERE E.first_name LIKE %?1%",nativeQuery = true)
    public  List<EmployeeModel> findByName(@Param("name") String name);

}
