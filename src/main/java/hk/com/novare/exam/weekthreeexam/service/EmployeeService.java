package hk.com.novare.exam.weekthreeexam.service;

import hk.com.novare.exam.weekthreeexam.model.AdminManagerModel;
import hk.com.novare.exam.weekthreeexam.model.EmployeeModel;
import hk.com.novare.exam.weekthreeexam.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EmployeeService {

    EmployeeModel employeeModel;

    @Autowired
    private EmployeeRepository employeeRepository;

    public void deleteEmployeeByName(String name){
        employeeRepository.deleteEmployeeByName(name);
    }

    public void deleteSingleEmployee(int id){
        employeeRepository.deleteSingleEmployee(id);
    }

    public void updateEmployee(EmployeeModel employeeModel,int id){

        EmployeeModel employeeModel1 = employeeRepository.findById(id).orElse(null);

        if (employeeModel.getFirstName() != null) {
            employeeModel1.setFirstName(employeeModel.getFirstName());
        }
        if (employeeModel.getLastName() != null) {
            employeeModel1.setLastName(employeeModel.getLastName());
        }
        if (employeeModel.getPosition() != null) {
            employeeModel1.setPosition(employeeModel.getPosition());
        }
        if (employeeModel.getDateHired() != null) {
            employeeModel1.setDateHired(employeeModel.getDateHired());
        }
        if (employeeModel.getAdminManagerModel() != null) {
            employeeModel1.setAdminManagerModel(employeeModel.getAdminManagerModel());
       }
       employeeRepository.save(employeeModel1);
    }

    public EmployeeModel addNewEmployee(EmployeeModel model) {
        return employeeRepository.save(model);
    }

    public List<EmployeeModel> findAllEmployee() {
        return employeeRepository.findAllEmployee();
    }
    public List<EmployeeModel> findEmployeeById(int id)
    {
        return employeeRepository.findEmployeeById(id);
    }

    public List<EmployeeModel> findBySalary(int id, int salary)
    {
        return employeeRepository.findBySalary(id,salary);
    }

    public List<EmployeeModel> findByName(String name){
        return employeeRepository.findByName(name);
    }




}
