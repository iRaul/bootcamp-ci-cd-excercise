package hk.com.novare.exam.weekthreeexam.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity(name = "employee_profile")
@Table(name = "employee_profile")
public class EmployeeModel {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "adminmanager_id")
    private AdminManagerModel adminManagerModel;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "dateHired")
    private Date dateHired;

    @Column(name = "position")
    private String Position;

    @Column(name = "salary")
    private int salary;


    public EmployeeModel(AdminManagerModel adminManagerModel, int id, String firstName, String lastName, Date dateHired, String position, int salary) {
        this.adminManagerModel = adminManagerModel;
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateHired = dateHired;
        Position = position;
        this.salary = salary;
    }
}
