package hk.com.novare.exam.weekthreeexam.repository;


import hk.com.novare.exam.weekthreeexam.model.AdminManagerModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminManagerRepository extends JpaRepository<AdminManagerModel, Integer> {
}
