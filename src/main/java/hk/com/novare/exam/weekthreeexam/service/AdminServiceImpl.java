package hk.com.novare.exam.weekthreeexam.service;
import hk.com.novare.exam.weekthreeexam.model.AdminManagerModel;
import hk.com.novare.exam.weekthreeexam.repository.AdminManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService{

    AdminManagerModel adminManagerModel;

    @Autowired
    private final AdminManagerRepository adminManagerRepository;

    public AdminServiceImpl(AdminManagerRepository adminManagerRepository) {
        this.adminManagerRepository = adminManagerRepository;
    }

    @Override
    public AdminManagerModel addNewAdmin(AdminManagerModel emp) {
        return adminManagerRepository.save(emp);
    }

    @Override
    public List<AdminManagerModel> getAllAdmin() {
        List<AdminManagerModel> model =  adminManagerRepository.findAll();
        return model;
    }
}
