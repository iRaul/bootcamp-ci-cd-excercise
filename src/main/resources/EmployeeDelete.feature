@tag
Feature: Employee Delete
  As a user
  I want to delete an employee from the database
  So that the non-existing and retired employees are
  Removed from the database

  @tag1
  Scenario: Delete an employee from database
    Given I have an employee database
    When I delete an employee with the corresponding attributes
    Then the employee will be removed from the database

