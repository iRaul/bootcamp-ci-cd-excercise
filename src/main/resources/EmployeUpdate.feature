@tag
Feature: Employee Update
  As a user
  I want to update an existing employee in the database
  So that employees that have changed or different information
  Are updated in the database

  @tag1
  Scenario: Update an employee in the database
    Given I have an employee database
    When I update an employee with the corresponding attributes
    Then the employee data will be updated in the database

