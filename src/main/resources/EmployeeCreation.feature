@tag
Feature: Employee Creation
  This feature will allow user to add employee

  @tag1
  Scenario: Successfully create an employee
    Given that the employee does not exist in the database
    And that the employee has a firstname, lastname, datehired, position and salary
    When the user triggers the add new employee function
    Then the system will saved the new employee to the database